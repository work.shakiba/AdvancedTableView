#
# Be sure to run `pod lib lint AdvancedTableView.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'AdvancedTableView'
  s.version          = '0.3.1'
  s.summary          = 'Replacement for UITableViewb'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
An alternative to UITableView with more features and easier api
                       DESC

  s.homepage         = 'https://gitlab.com/work.shakiba/AdvancedTableView'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'mohsenShakiba' => 'work.shakiba@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/work.shakiba/AdvancedTableView.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'AdvancedTableView/Classes/**/*'
  
  # s.resource_bundles = {
  #   'AdvancedTableView' => ['AdvancedTableView/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'

    s.dependency 'RxSwift', '~> 3.0'

end
