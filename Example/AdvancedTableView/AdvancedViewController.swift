//
//  AdvancedViewController.swift
//  AdvancedTableView
//
//  Created by mohsen shakiba on 3/27/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import Foundation
import AdvancedTableView

class AdvancedViewController: AdvancedTableViewController {
    
    override func setup() {
        
//        row(LabelRow.self)
//        .setup { (row) in
//            row.text = "back"
//        }
//            .click {
//                self.dismiss(animated: true, completion: nil)
//            }
//        .commit()
//        
//        row(FormRow.self)
//            .setup({ (row) in
//                row.title = "test one"
//                row.value = "value one"
//            })
//            .commit("form")
//        
//        row(FormRow.self)
//            .setup({ (row) in
//                row.title = "test two"
//                row.value = "value two"
//            })
//            .commit("form")
//        
//        row(LabelRow.self)
//            .setup({ (row) in
//                row.text = "Click to validate"
//            })
//            .click {
//                self.validate()
//            }
//            .commit()
    }
    
    func validate() {
        guard let form = rowBy(id: "form") as? FormRow else { return }
        let (model, err) = form.validate()
    }
    
}
