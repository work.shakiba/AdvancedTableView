//
//  ViewController.swift
//  AdvancedTableView
//
//  Created by mohsenShakiba on 06/15/2017.
//  Copyright (c) 2017 mohsenShakiba. All rights reserved.
//

import UIKit
import AdvancedTableView

class ViewController: AdvancedTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.preventDispose = true
    }
    
    override func setup() {
        
        section { (s) in
            
            LabelRow(text: "hi i'm here")
                .onDisplay({ (row) in
                    row.cell?.label.textColor = .blue
                })
                .click {
                    print("clicked")
                }
                .commit(s)
            
            LabelRow(text: "Height Test")
                .click {
                    let vc = HeightTestViewController()
                    self.present(vc, animated: true, completion: nil)
                }
                .commit(s)

            LabelRow(text: "Dynamic Test")
                .click {
                    let vc = DynamicTableView()
                    self.present(vc, animated: true, completion: nil)
                }
                .commit(s)
            
            }
            .finalize()
        
        
        
    }
    
}


