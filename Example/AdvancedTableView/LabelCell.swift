//
//  LabelCell.swift
//  AdvancedTableView
//
//  Created by mohsen shakiba on 3/25/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import AdvancedTableView

class LabelCell: Cell {

    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        label.numberOfLines = 99
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTap))
//        label.addGestureRecognizer(tapGesture)
//        label.isUserInteractionEnabled = true
    }
    
    func onTap() {
        self.emit(event: "test", value: true)
    }
}

class LabelRow: Row<LabelCell> {
    
    var textAlignment: NSTextAlignment = .right
    var textColor: UIColor = .black
    var text: String
    
    init(text: String) {
        self.text = text
    }
    
    override func setup() {
        self.cell?.label.text = text
        self.cell?.label.textColor = textColor
        self.cell?.label.textAlignment = textAlignment
    }
    
    override func height() -> CGFloat {
        let label = UILabel()
        label.numberOfLines = 99
        label.text = text
        let width = UIScreen.main.bounds.width - (2 * 16)
        let height = CGFloat.infinity
        let size = CGSize(width: width, height: height)
        return label.sizeThatFits(size).height + 32
    }
    
}
