////
////  BooksViewController.swift
////  ListView
////
////  Created by mohsen shakiba on 2/6/1396 AP.
////  Copyright © 1396 CocoaPods. All rights reserved.
////
//
import Foundation
import AdvancedTableView


class HeightTestViewController: AdvancedTableViewController {
    
    var cellId: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setContent()
    }
    
    deinit {
        print("deinit...")
    }
    
    func setContent() {
        
        section { (s) in
            LabelRow(text: "back")
                .onDisplay { (row) in
                    row.textColor = .red
                }
                .click{
                    self.dismiss(animated: true, completion: nil)
                }
                .commit(s)
            
            LabelRow(text: "insert items")
                .onDisplay { (row) in
                    row.textColor = .blue
                }
                .click{
                    self.addItem()
                }
                .commit(s)
        }
        
        
        section { (s) in
            }
            .finalize("list")
        
        more()
    }
    
    func more() {
        self.beginUpdates()
        guard let list = sectionBy(id: "list") else { return }
        for i in 0...10 {
            let randomInt = randomIntFrom(start: 10, to: 500)
            let str  = randomString(length: randomInt)
            LabelRow(text: str)
                .commit(list)
        }
        self.endUpdates(false)
    }
    
    func addItem() {
        guard let list = sectionBy(id: "list") else { return }
        let id = randomString(length: 10)
        LabelRow(text: id)
            .commit(list)
        self.cellId = id
    }
    
    func modifyItem(id: String) {
        reloadRowBy(id: cellId) { (row: LabelRow) in
            row.textColor = .green
            row.text = randomString(length: 25)
        }
    }
    
}


func randomString(length: Int) -> String {
    
    let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    let len = UInt32(letters.length)
    
    var randomString = ""
    
    for _ in 0 ..< length {
        let rand = arc4random_uniform(len)
        var nextChar = letters.character(at: Int(rand))
        randomString += NSString(characters: &nextChar, length: 1) as String
    }
    
    return randomString
}

func randomIntFrom(start: Int, to end: Int) -> Int {
    var a = start
    var b = end
    // swap to prevent negative integer crashes
    if a > b {
        swap(&a, &b)
    }
    return Int(arc4random_uniform(UInt32(b - a + 1))) + a
}
