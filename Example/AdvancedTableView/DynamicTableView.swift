//
//  DynamicTableView.swift
//  AdvancedTableView
//
//  Created by mohsen shakiba on 4/30/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import AdvancedTableView
import RxSwift

struct User: Identifiable {
    
    var id: Int {
        return self.content.hashValue
    }
    
    var content: String
}


var baseRepo = AdvancedSet<User>()
var filteredRepo = baseRepo.slice(filter: {item in
    return item.content.characters.count == 2
}, sort: {(first, second) in
    return Int(first.content)! < Int(second.content)!
})


class DynamicTableView: AdvancedTableViewController, DynamicListProtocol {
    
    var advancedSet: AdvancedSet<User> {
        get { return filteredRepo }
    }
    
    var advancedTableViewController: AdvancedTableViewController {
        get { return self }
    }
    
    var _disposeBag = DisposeBag()
    
    var disposeBag: DisposeBag {
        get { return self._disposeBag }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialize()
    }
    
    override func setup() {
        
        section { (s) in
            LabelRow(text: "add")
                .click {
                    let user = User(content: String(randomNumber(min: 0, max: 150)))
                    baseRepo.add(item: user)
                }
                .commit(s)
            }
            .finalize()
        
    }
    
    func row(for item: User, at: Int) -> RowProtocol {
        let row = LabelRow(text: item.content).click {
            baseRepo.remove(by: item)
        }
        return row
    }
    
}

func randomNumber(min: UInt32, max: UInt32) -> Int {
    return Int(arc4random_uniform(max) + min)
}
