
//
//  TestViewController.swift
//  AdvancedTableView
//
//  Created by mohsen shakiba on 3/30/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit

class TestViewController: UIViewController {

    @IBOutlet weak var textView: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.becomeFirstResponder()
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
