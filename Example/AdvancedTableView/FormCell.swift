//
//  FormCell.swift
//  AdvancedTableView
//
//  Created by mohsen shakiba on 3/28/1396 AP.
//  Copyright © 1396 CocoaPods. All rights reserved.
//

import UIKit
import AdvancedTableView

class FormCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}


class FormRow: Row<FormCell>, UITextViewDelegate  {
    
    var title: String = ""
    var placeHolder: String?
    var value: String?
    var rules: [ValidationProtocol] = []
    var error: String?
    
    override func setup() {
        cell?.titleLabel.text = title
        cell?.textView.text = value
        cell?.textView.isUserInteractionEnabled = false
        cell?.textView.delegate = self
        changeStatusAccordingToStats()
    }
    
    override func rowSelected() {
        cell?.textView.isUserInteractionEnabled = true
        cell?.textView.becomeFirstResponder()
    }
    
    override func rowDeselected() {
        cell?.textView.isUserInteractionEnabled = false
        cell?.textView.resignFirstResponder()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        value = textView.text
    }
    
    func validate() -> (value: String, error: String?) {
        let res = validateCell()
        changeStatusAccordingToStats()
        return res
    }
    
    private func changeStatusAccordingToStats() {
        if error != nil {
            cell?.titleLabel.text = error
            cell?.backgroundColor = UIColor(white: 0.9, alpha: 1)
        }else{
            cell?.titleLabel.text = title
            cell?.backgroundColor = .white
        }
    }
    
    
    override func height() -> CGFloat {
        let label = UITextView()
        label.text = value
        let width = UIScreen.main.bounds.width - (2 * 16)
        let height = CGFloat.infinity
        let size = CGSize(width: width, height: height)
        return label.sizeThatFits(size).height + 25 + 32
    }
    
    func validateCell() -> (value: String, error: String?) {
        for rule in rules {
            if let error = rule.error(str: self.value) {
                self.error = error
                return (self.value ?? "", error)
            }
        }
        error = nil
        return (value ?? "", nil)
    }
    
    
    func addValidation(rule: ValidationProtocol) {
        self.rules.append(rule)
    }
    
    override func keyboardVisible() -> Bool {
        self.cell?.textView.becomeFirstResponder()
        return true
    }
    
}


public protocol ValidationProtocol {
    
    /// returns nil if the validation succeeds and the error message if the the validation fails
    func error(str: String?) -> String?
    
}

class StringValidationRule: ValidationProtocol {
    
    var min: Int = 0
    var max: Int = 999
    
    init(min: Int, max: Int) {
        self.min = min
        self.max = max
    }
    
    func error(str: String?) -> String? {
        if let str = str {
            if str.characters.count < min {
                return "min char not  met"
            }else if str.characters.count > max {
                return "mac char not met"
            }
            return nil
        }else{
            return nil
        }
    }
    
    
    
}
