////
////  CrazyViewController.swift
////  ListView
////
////  Created by mohsen shakiba on 2/13/1396 AP.
////  Copyright © 1396 CocoaPods. All rights reserved.
////
//
//import Foundation
//import AdvancedTableView
//
//class CrazyViewController: AdvancedTableViewController {
//    
//    
//    
//   override func setup() {
//        
//        row(LabelRow.self)
//            
//            
//        .click { (_) in
//            self.dismiss(animated: true, completion: nil)
//            }
//        .commit()
//        
//        row(LabelRow.self)
//            .setup({ row in
//                row.text = "GO CRAZY"
//            })
//            .click { (_) in
//                self.initCrazy()
//            }
//            .commit()
//        
//        for i in 0...9 {
//            section({ (s) in
//                for l in 0...9 {
//                    s.row(LabelRow.self)
//                        .setup({ row in
//                            row.text = String(l + (i * 10))
//                        })
//                    .commit(String(l + (i * 10)))
//                }
//            })
//                .finalize(String(i))
//        }
//    }
//    
//    func initCrazy() {
//        Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(CrazyViewController.crazyOperation), userInfo: nil, repeats: true)
//    }
//    
//    func crazyOperation() {
//        self.beginUpdates()
//        for i in 0...10 {
//            let id = String(i)
//                if let section = self.sectionBy(id: id) {
//                    let ran = getRandomNumber(0, 10)
//                    if ran % 2 == 0 {
//                        section.hide()
//                    }else{
//                        section.show()
//                    }
//                }
//        }
//        for i in 0...100 {
//            let id = String(i)
//            
//            if let cell = self.rowBy(id: id) {
//                let ran = getRandomNumber(0, 25) % 4
//                if ran == 0 {
//                    cell.hide()
//
//                }else if ran == 1 {
//                    cell.show()
//                }
//                else if ran == 2 {
//                    cell.remove()
//                }
//                else if ran == 3 {
//                    let randomSectionID = getRandomNumber(0, 9)
//                    let section = sectionBy(id: String(randomSectionID))
//                    section?.row(LabelRow.self)
//                        .setup({ row in
//                            row.text = "Added row"
//                        })
//                    .commit()
//                }
//            }
//        }
//        self.endUpdates(false)
//    }
//    
//    private func getRandomNumber(_ min: UInt32, _ max: UInt32) -> Int {
//        let number = arc4random_uniform((max - min) + 1) + min
//        return Int(number)
//    }
//    
//}
