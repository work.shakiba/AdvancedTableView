//
//  Cell.swift
//  Pods
//
//  Created by mohsen shakiba on 4/13/1396 AP.
//
//

import UIKit
import RxSwift

open class Cell: UITableViewCell {
    
    weak var row: BaseRow?
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
    }
    
    open override func setSelected(_ selected: Bool, animated: Bool) {
        self.selectionStyle = .none
    }
    
    open func highlightColor() -> UIColor {
        return UIColor.clear
    }
    
    open func borderInset() -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    open func borderColor() -> UIColor {
        return UIColor(white: 0, alpha: 0.05)
    }
    
    public func emit(event: String, value: Any?) {
        row?.emit(event: event, value: value)
    }
    
}


