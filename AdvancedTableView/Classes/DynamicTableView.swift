//
//  DynamicTableView.swift
//  Pods
//
//  Created by mohsen shakiba on 4/30/1396 AP.
//
//

import Foundation
import RxSwift

public protocol Identifiable {
    
    var id: Int { get }
    
}

extension Identifiable {
    
    static func ==(lhs: Self, rhs: Self) -> Bool {
        return lhs.id == rhs.id
    }
    
}

public enum Modification<T> where T: Identifiable {
    case remove(item: T, at: Int)
    case add(item: T, at: Int)
    case update(item: T, at: Int)
    case clear
}


open class AdvancedSet<T>: Collection, CustomStringConvertible where T: Identifiable {
    
    private var disposeBag = DisposeBag()
    private var set = [T]()
    private var sortPredicate: ((T, T) -> Bool)?
    private var filterPredicate: ((T) -> Bool)?
    private var subscriptor = PublishSubject<[Modification<T>]>()
    
    /// default initializer
    public init(items: [T], filter: ((T) -> Bool)?, sort: ((T, T) -> Bool)?) {
        self.set = items.filter({ (item) -> Bool in
            return filter?(item) ?? true
        })
        self.filterPredicate = filter
        self.sortPredicate = sort
    }
    
    /// conveneince intitalizer for empty set
    public convenience init() {
        self.init(items: [], filter: nil, sort: nil)
    }
    
    /// method allows for subscription
    public func subscribe(handler: @escaping ([Modification<T>]) -> Void) -> Disposable {
        return self.subscriptor.subscribe(onNext: handler, onError: nil, onCompleted: nil, onDisposed: nil)
    }
    
    /// set will subscribe to the given set
    public func subscripeTo(set: AdvancedSet<T>) {
        set.subscribe { modifications in
            for modification in modifications {
                self.process(modification: modification)
            }
            }.addDisposableTo(disposeBag)
    }
    
    /// if set contains the given value
    public func contains(item: T) -> Bool {
        if set.contains(where: {$0.id == item.id}) {
            return true
        }
        return false
    }
    
    /// add item according to given sort and filter predicate
    public func add(item: T) {
        if !meetsCriteria(item: item) { return }
        if self.contains(item: item) { return }
        set.append(item)
        sortByPredicate()
        notifiyModification(item: item, type: .add)
    }
    
    /// will remove all the items
    public func clear() {
        self.set.removeAll()
        self.notifiyReload()
    }
    
    /// remove the item at the given position
    public func remove(at position: Int) {
        if !indexIsValid(index: position) { return }
        let item = self.set[position]
        notifiyModification(item: item, type: .remove)
        self.set.remove(at: position)
    }
    
    /// remove the item
    public func remove(by item: T) {
        if let index = set.index(where: {$0.id == item.id}) {
            let item = self.set[index]
            notifiyModification(item: item, type: .remove)
            self.set.remove(at: index)
        }
    }
    
    /// update the item and sort and filter with the given predicate
    public func update(item: T) {
        if !self.meetsCriteria(item: item) { return }
        if let index = set.index(where: {$0.id == item.id}) {
            self.set[index] = item
            sortByPredicate()
            notifiyModification(item: item, type: .update)
        }
    }
    
    /// get subSet with the given predicate
    public func slice(filter: ((T) -> Bool)?, sort: ((T, T) -> Bool)?) -> AdvancedSet<T> {
        let set = AdvancedSet(items: self.set, filter: filter, sort: sort)
        set.subscripeTo(set: self)
        return set
    }
    
    public func item(index: Int) -> T? {
        if index >= self.set.count { return nil }
        return self.set[index]
    }
    
    public func item(hash: Int) -> T? {
        return self.set.first(where: {$0.id == hash})
    }
    
    public var description: String {
        return self.set.description
    }
    
    public var startIndex: Int {
        get { return 0 }
    }
    
    public var endIndex: Int {
        get { return self.set.count }
    }
    
    public func index(after i: Int) -> Int {
        return i + 1
    }
    
    public func makeIterator() -> AdvancedSetIterator<T> {
        return AdvancedSetIterator(index: 0, items: self.set)
    }
    
    public subscript(position: Int) -> T {
        get {
            return self.set[1]
        }
    }
    
    private func process(modification: Modification<T>) {
        if case Modification.add(item: let item, at: _) = modification {
            self.add(item: item)
        }else if case Modification.remove(item: let item, at: _) = modification {
            self.remove(by: item)
        }else if case Modification.update(item: let item, at: _) = modification {
            self.update(item: item)
        }else if case Modification<T>.clear = modification {
            self.clear()
        }
    }
    
    private func indexIsValid(index: Int) -> Bool {
        // if index out of range
        if index >= set.count {
            throwFatalErrorIfNeeded(msg: "index out of range")
            return false
        }
        // if index negetive
        if index < 0 {
            throwFatalErrorIfNeeded(msg: "index negative")
            return false
        }
        return true
    }
    
    private func throwFatalErrorIfNeeded(msg: String) {
        fatalError(msg)
    }
    
    private func notifiyModification(item: T, type: SubscriptionModificationType) {
        if let index = self.set.index(where: {$0.id == item.id}) {
            let modification = type.modification(item: item, at: index)
            self.subscriptor.onNext([modification])
        }
    }
    
    private func notifiyReload() {
        let modification = Modification<T>.clear
        self.subscriptor.onNext([modification])
    }
    
    private func meetsCriteria(item: T) -> Bool {
        if !(filterPredicate?(item) ?? true) {
            return false
        }
        return true
    }
    
    private func sortByPredicate() {
        if let sortPredicate = self.sortPredicate {
            self.set.sort(by: sortPredicate)
        }
    }
    
    private enum SubscriptionModificationType {
        case add, remove, update, clear
        
        func modification(item: T, at: Int) -> Modification<T> {
            switch self {
            case .add:
                return Modification.add(item: item, at: at)
            case .remove:
                return Modification.remove(item: item, at: at)
            case .update:
                return Modification.update(item: item, at: at)
            case .clear:
                return Modification.clear
            }
        }
    }
    
}

public struct AdvancedSetIterator<T>: IteratorProtocol {
    
    var index: Int = 0
    var items: [T] = []
    
    mutating public func next() -> T? {
        defer { index += 1 }
        if index >= self.items.count { return nil }
        return items[index]
    }
    
}

public protocol DynamicListProtocol {
    
    associatedtype T: Identifiable
    
    /// return instance of AdvanceSet
    var advancedSet: AdvancedSet<T> { get }
    var advancedTableViewController: AdvancedTableViewController { get }
    var disposeBag: DisposeBag { get }
    
    /// return a row for the given model
    func row(for item: T, at: Int) -> RowProtocol
        
}

public extension DynamicListProtocol {
    
    /// to be called on viewDidLoad
    func initialize() {
        advancedSet.subscribe { (modifications) in
            self.process(modifications)
            }.addDisposableTo(disposeBag)
        setupListSection()
    }
    
    /// to create the section for the given list
    func setupListSection() {
        advancedTableViewController.section().finalize("list", at: nil)
    }
    
    /// function that returns
    func getListSection() -> Section {
        return advancedTableViewController.sectionBy(id: "list")!
    }
    
    /// process the modifications
    func process(_ modifications: [Modification<T>]) {
        advancedTableViewController.beginUpdates()
        defer {
            list(updated: modifications)
            advancedTableViewController.endUpdates()
        }
        for modification in modifications {
            if case Modification.add(item: let item, at: let at) = modification {
                self.onAdd(item: item, at: at)
            }else if case Modification.remove(item: let item, at: let at) = modification {
                self.onRemove(item: item, at: at)
            }else if case Modification.update(item: let item, at: let at) = modification {
                self.onUpdate(item: item, at: at)
            }
        }
    }
    
    /// on item add
    func onAdd(item: T, at: Int) {
        let section = getListSection()
        row(for: item, at: at)
            .at(at)
            .id(String(item.id))
            .commit(section)
    }
    
    /// on item remove
    func onRemove(item: T, at: Int) {
        let section = getListSection()
        section.removeRowBy(index: at)
    }
    
    /// on item update
    func onUpdate(item: T, at: Int) {
        let section = getListSection()
        section.reloadRowBy(index: at)
    }
    
    /// on click event
    func row(click item:T, at: Int) {
        
    }
    
    /// on before updates
    func list(updated with: [Modification<T>]) {
        
    }
    
}

