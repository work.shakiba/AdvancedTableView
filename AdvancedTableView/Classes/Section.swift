//
//  Section.swift
//  Pods
//
//  Created by mohsen shakiba on 3/25/1396 AP.
//
//

open class Section: CoreSection {
    
    public var identifier: String {
        get {
            return self.id
        }
    }
    
    // footer height
    public var footerHeight: CGFloat {
        get { return self._footerHeight }
        set { self._footerHeight = newValue }
    }
    
    // header height
    public var headerHeight: CGFloat {
        get { return self._headerHeight }
        set { self._headerHeight = newValue }
    }
    
    // footer height
    public var footerView: UIView? {
        get { return self._footerView }
        set { self._footerView = newValue }
    }
    
    // header height
    public var headerView: UIView? {
        get { return self._headerView }
        set { self._headerView = newValue }
    }
    
    
    // return visible rows
    public var visibleRows: [BaseRow] {
        get { return repository.visibleItems }
    }
    
    // get all cell
    public var allRows:[BaseRow] {
        get { return repository.allItems }
    }
    
    // find cell by id
    public override func rowBy(id: String) -> BaseRow? {
        return super.rowBy(id: id)
    }
    
    // return the cell as the type specified
    public func cellBy<T>(id: String) -> T? {
        return rowBy(id: id)?.innerCell as? T
    }
    
    // finalize the section
    public func finalize(_ id: String? = nil, at: Int? = nil) {
        let id = id ?? String.randomString(length: 5)
        self.id = id
        self.controller?.commitSection(section: self, at: at)
    }
    
    // show this section if needed
    public func show() {
        if isVisible() { return }
        self.controller?.beginUpdatesIfNeeded()
        self.nextOperation = .changoToVisible
        self.controller?.endUpdatesIfNeeded()
    }
    
    // hide this section if needed
    public func hide() {
        if !isVisible() { return }
        self.controller?.beginUpdatesIfNeeded()
        self.nextOperation = .changeToHidden
        self.controller?.endUpdatesIfNeeded()
    }
    
    // remove this section
    public func remove() {
        self.controller?.removeSectionBy(id: self.id)
    }
    
    // reload section
    public func reload() {
        self.controller?.reloadSectionBy(id: self.id)
    }
    
    // insert row
//    public func row<RowT, Cell>(_ type: RowT.Type) -> RowTransferObject<RowT, Cell> where RowT: Row<Cell>, Cell: UITableViewCell, Cell: UITableViewCell {
//        let bundle = Bundle(for: Cell.classForCoder())
//        let nib = UINib.fromType(type: Cell.self, bundle: bundle)
//        let nibName = UINib.name(type: Cell.self)
//        let id = String.randomString(length: 10)
//        let master = RowT()
//        master.section = self
//        master.reuseIdentifier = nibName
//        master.id = id
//        let transferObject = RowTransferObject<RowT, Cell>(master)
//        controller?.tableView.register(nib, forCellReuseIdentifier: nibName)
//        return transferObject
//    }
    
    // remove all cells
    public func clear() {
        self.beginUpdatesIfNeeded()
        for s in allRows {
            s.nextOperation = .changeToRemove
        }
        self.endUpdatesIfNeeded()
    }
    
    // reload row by id
    public func reloadRowBy<T>(id: String, _ handler: ((T) -> Void)? = nil) {
        guard let sectionIndex = self.index else { return }
        let newHandler: (RowProtocol) -> Void = { row in
            handler?(row as! T)
        }
        if let rowIndex = rowIndexBy(id: id) {
            let row = rowBy(id: id)
            row?.nextUpdateHandler = newHandler
            self.beginUpdatesIfNeeded()
            let indexPath = IndexPath(item: rowIndex, section: sectionIndex)
            self.controller?._tableView.reloadRows(at: [indexPath], with: .fade)
            self.endUpdatesIfNeeded()
        }
    }
    
    public func reloadRowBy(id: String) {
        guard let sectionIndex = self.index else { return }
        if let rowIndex = rowIndexBy(id: id) {
            self.beginUpdatesIfNeeded()
            let indexPath = IndexPath(item: rowIndex, section: sectionIndex)
            self.controller?._tableView.reloadRows(at: [indexPath], with: .fade)
            self.endUpdatesIfNeeded()
        }
    }
    
    public func reloadRowBy(index: Int) {
        guard let sectionIndex = self.index else { return }
        let indexPath = IndexPath(item: index, section: sectionIndex)
        self.beginUpdatesIfNeeded()
        self.controller?._tableView.reloadRows(at: [indexPath], with: .fade)
        self.endUpdatesIfNeeded()
    }
    
    public override func removeRowBy(id: String) {
        super.removeRowBy(id: id)
    }
    
    public override func removeRowBy(index: Int) {
        super.removeRowBy(index: index)
    }
    
}

open class CoreSection: VisibleItem {
    
    
    // header and footer height
    var _footerHeight: CGFloat = 0.1
    var _headerHeight: CGFloat = 0.1
    
    // header and footer view
    var _footerView: UIView?
    var _headerView: UIView?
    
    // next operation for section
    var nextOperation: NextOperation = .changeToCommit
    var nextOperationIndex: Int?
    
    
    // section identifier
    var id: String!
    
    // index of this section in table view
    var index: Int? = nil
    
    // weak referenced pointer to controller
    weak var controller: AdvancedTableViewController?
    
    let repository = Repository<BaseRow>()
    
    // config handler
    // this handler is called once the section is finalized
    // once handler is called, it is removed
    var configHandler: ((Section) -> Void)?
    
    // if repository needs to rebuild the visible cells
    // needed to more efficently create cache
    var needsCacheInvalidation = false
    
    // reached bottom status
    var reachedBottomRowIndex = -1
    
    // default initializer
    init(id: String, controller: AdvancedTableViewController) {
        self.id = id
        self.controller = controller
    }
    
    // find row by row
    func rowBy(index: Int) -> BaseRow {
        return repository.visibleItems[index]
    }
    
    // current visibllity status of section wihtout changing the lastVisibilityState
    func isVisible() -> Bool {
        if nextOperation == .visible || nextOperation == .changeToCommit || nextOperation == .changoToVisible {
            return true
        }
        return false
    }
    
    // row reuseIdentifier
    func cellReuseIdentifierBy(row: Int) -> String? {
        return repository.visibleItems[row].reuseIdentifier
    }
    
    // get row height
    func rowHeightBy(row: Int) -> CGFloat {
        let row = self.repository.visibleItems[row]
        let height = row.height()
        return height
    }
    
    // section header height
    func getHeaderHeight() -> CGFloat {
        if self.repository.visibleItems.count == 0 {
            return 0.1
        }
        return self._headerHeight
    }
    
    // section footer height
    func getFooterHeight() -> CGFloat {
        if repository.visibleItems.count == 0 {
            return 0.1
        }
        return self._footerHeight
    }
    
    // heade view
    func getHeaderView() -> UIView? {
        return _headerView
    }
    
    // footer view
    func getFooterView() -> UIView? {
        return _footerView
    }
    
    // clear all references for this section
    func dispose() {
        self.configHandler = nil
        for row in self.repository.allItems {
            row.dispose()
        }
    }
    
    // if row is the last row in section
    func didReachBottom(row: Int) -> Bool {
        // count of visible rows
        let rowCount = self.repository.visibleItems.count
        // if row is less than the row count
        if row < rowCount - 1 {
            return false
        }
        // if row is less than the last
        if row <= reachedBottomRowIndex {
            return false
        }
        // set the reached bottom index
        reachedBottomRowIndex = row
        return true
    }
    
    func rowIndexBy(id: String) -> Int? {
        return repository.visibleItems.index(where: {$0.id == id})
    }
    
    // commit row
    func accept(row: BaseRow, at: Int? = nil, asHidden: Bool) {
        #if DEBUG
            if let row = rowBy(id: row.id) {
                if row.nextOperation != .changeToRemove {
                    fatalError("duplicate id detected for section \(self.id) the id is \(row.id)")
                }
            }
        #endif
        beginUpdatesIfNeeded()
        if asHidden {
            row.nextOperation = .hidden
        }
        _ = repository.insert(row: row, at: at)
        endUpdatesIfNeeded()
    }
    
    func removeRowBy(id: String) {
        guard let row = rowBy(id: id) else {
            return
        }
        beginUpdatesIfNeeded()
        row.nextOperation = .changeToRemove
        endUpdatesIfNeeded()
    }
    
    func removeRowBy(index: Int) {
        let row = repository.items[index]
        beginUpdatesIfNeeded()
        row.nextOperation = .changeToRemove
        endUpdatesIfNeeded()
    }
    
    func beginUpdatesIfNeeded() {
        self.controller?.beginUpdatesIfNeeded()
    }
    
    func endUpdatesIfNeeded() {
        self.controller?.endUpdatesIfNeeded()
    }
    
    func rowBy(id: String) -> BaseRow? {
        return repository.allItems.first(where: {$0.id == id})
    }
    
    func updateIfNeeded(index: Int?, next: Int?) {
        let beforeUpdateRows = self.repository.visibleItems.count
        var numberOfInserts = 0
        var numberOfDeleted = 0
        let rowsToHide = repository.itemsToDelete()
        for row in rowsToHide {
            if let noi = row.nextOperationIndex {
                if let sectionIndex = index {
                    numberOfDeleted += 1
                    let indexPath = IndexPath(item: noi, section: sectionIndex)
                    logEvent("deleting row at index \(noi) as \(row.nextOperation)")
                    self.controller?._tableView.deleteRows(at: [indexPath], with: .fade)
                }
            }
        }
        let rowsToCommit = repository.itemsToInsert()
        for row in rowsToCommit {
            if let noi = row.nextOperationIndex {
                if let sectionIndex = next {
                    numberOfInserts += 1
                    let indexPath = IndexPath(item: noi, section: sectionIndex)
                    logEvent("inserting row at index \(noi) as \(row.nextOperation)")
                    self.controller?._tableView.insertRows(at: [indexPath], with: .fade)
                }
            }
        }
        self.repository.invalidateCache(log: false)
        logEvent("update finished for section: \(self.id) the index is current: \(index ?? -1), next: \(next ?? -1) inserts: \(numberOfInserts) deletes: \(numberOfDeleted) before: \(beforeUpdateRows) after: \(self.repository.visibleItems.count)")
    }
    
    func configIfNeeded() {
        self.beginUpdatesIfNeeded()
        if self.configHandler != nil {
            self.configHandler?(self as! Section)
            self.configHandler = nil
        }
        self.endUpdatesIfNeeded()
    }
    
}
