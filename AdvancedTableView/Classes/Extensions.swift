//
//  UINibExtenion.swift
//  Pods
//
//  Created by mohsen shakiba on 12/30/1395 AP.
//
//

import Foundation
import UIKit

extension UINib {
    
    static func fromType(type: AnyClass, bundle: Bundle) -> UINib {
        let nibName = name(type: type)
        return UINib(nibName: nibName, bundle: bundle)
    }
    
    static func name(type: AnyClass) -> String {
        let nibName = NSStringFromClass(type).components(separatedBy: ".").last
        return nibName!
    }
    
}

extension UIView {
    
    func borderIfNeeded(_ edge: UIRectEdge, color: UIColor, inset: UIEdgeInsets) {
        
        let tag = edge == .top ? 1111 : 1112
        
        if let _ = subviews.index(where: {$0.tag == tag}) {
            return
        }
        subviews.forEach { layer in
            if layer.tag == tag {
                return
            }
        }
        
        let border = UIView()
        border.tag = tag
        border.frame = CGRect(x: inset.left, y: 0, width: self.bounds.width - (inset.left + inset.right), height: 1)
        border.backgroundColor = color
        border.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(border)
        
        let constraint = NSLayoutConstraint(item: border, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: inset.left)
        let constraint1 = NSLayoutConstraint(item: border, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: inset.right)
        let constraint3 = NSLayoutConstraint(item: border, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 1)
        
        var constraint2: NSLayoutConstraint!
        
        if edge == .top {
            constraint2 = NSLayoutConstraint(item: border, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        }else{
            constraint2 = NSLayoutConstraint(item: border, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
        }
        
        
        NSLayoutConstraint.activate([constraint, constraint1, constraint2, constraint3])
        
    }
    
}


extension String {
    
    static func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
}
